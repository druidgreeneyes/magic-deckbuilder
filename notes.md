### First playthrough

2 players, 50% experienced with MtG.

no lands, opening decks are drafted from the market. Hand size is 3.

Result: Game result heavily dependent on opening draft; later decisions had little impact on outcome.

Change: Add lands; starting deck is now 2 lands of each color, lands can be discarded for one mana of their color.

### Second playthrough

4 players, 50% experienced with MtG, free-for-all.

Result: SLOOOOOOOWWWW. Most expensive things on the field carried the game with no real resistance. No back-and-forth, no real player interaction.

Change: Hand size up to 5.

Suggested Changes:

*   Starting deck to be composed of 1-cost cards (preferrably creatures) instead of lands.
*   Market to be ordered by cost instead of randomized (2 piles of 1-cost, 2 piles of 2-cost, 2 piles of 3-cost, 1 pile each of 4, 5, 6, etc.)
*   Add a mechanism to get lands into play permanently, for reliable access to preferred mana colors.
*   Balance the deck better.
*   Remove flip-cards.
