# Magic: Wells of Infinite Power [WIP]

## What is this?

WIP is a reimagining of Magic: The Gathering as an on-the-fly deckbuilding game, in the vein of Hero Realms or Dominion. It is conveniently also a Work In Progress.

## How does it work?

If you're familiar with MtG, most of the core rules are the same; I've changed only what I had to in order to account for on-the-fly instead of prepared deckbuilding. The biggest differences:

*  You don't bring your own deck.
*  Lands don't get played, only discarded. Mostly.
*  Draw phase happens at the end of the turn, instead of the beginning.

Some things that are the same:

*  Summoning sickness.
*  Card rules take priority over game rules.
*  The Stack.

## Where do I start?

First get some cards. If you don't already have some, go to your local magic shop and tell them you want a couple hundred bulk cards, condition isn't important, all from the same set or block (you don't care which one), in a roughly even distribution of colors, without *too* many duplicates. You should be able to get this for under $5; if not, either go somewhere else or order it from ebay. Next, you need forty or fifty basic lands, set/condition doesn't matter, in an even distribution of colors (2 per color per player, up to 5 players). This should also cost you under $5. I got started for $4.50 all told.

Next seperate your cards into small  'packs', 5-ish cards per. Realistically you can do this however you want; I did it by looking for mechanical or thematic ties: 10 cards with Delerium, 7 that deal with Vampires, 2 that come back from the graveyard, etc. Don't include more than one of anything. My initial distribution is below. Seperate the lands into 10-card starting decks, 2 per color each.

Now, either lay out your packs and choose some of them to play from (probably will want at least 50 cards total) or just take them all, put them into a deck and shuffle.

Divide the main deck into a number of piles; I don't have a good handle on what a good number is yet, aside from the general advice that more piles will provide a greater number of options during the game, and less piles will -probably- make the game take longer. Put the piles face-up in the middle of the table where all players can reach them.

Each player takes a starting deck (10 lands, 2 of each color) and shuffles it. Each player draws 5 cards from his/her starting deck.

Decide who will go first.

Play.

## How to play?

Follow MtG's turn structure, with the following exeptions:

*  The [Draw Step](https://en.wikipedia.org/wiki/Magic:_The_Gathering_rules#Parts_of_a_turn) now happens as the last step during the [End Phase](https://en.wikipedia.org/wiki/Magic:_The_Gathering_rules#Ending_phase).
*  During the [Draw Step](https://en.wikipedia.org/wiki/Magic:_The_Gathering_rules#Parts_of_a_turn), if you have less than 5 cards in hand, draw until you have 5 cards. If you have 5 or more cards in hand, draw 1 card.
*  During either Action Phase, you may purchase cards from the Market (those piles in the middle of the table). Do do this, pay that card's mana cost, take it from the market, and place it into *your graveyard*. You generally won't get to use it until you have cycled through your deck.

And with a couple of additional rules:

*   To get mana during a turn, you may discard any card or cards from your hand to get its mana cost in mana:
    
    > Jane discards [Raging Goblin](http://mtg.wikia.com/wiki/Raging_Goblin) and two Plains cards. She now has RWW (one red and two white) in her mana pool, and can spend it on whatever she chooses.

*   A basic land may be discarded for one mana of its color.
*   Only non-basic lands may be put into play.
*   There is no Mana Burn.
    
    
## Goals:

*  Standardized pack lists
*  Co-op mechanics with boss decks a'la Sentinels of the Multiverse
*  Player Character mechanics with unique starting decks (Play as Nicol Bolas! Play as Jace Belleren! Play as [Atogatog](http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=31834)?)
*  I'm not sure about the exile-to-play-lands mechanic. Playtest feedback would be great.

## I have an idea! How can I tell you about it?

Great! I love ideas! First, test your idea. Then create an issue and tell me what the motivation is behind your idea ('I think discard-for-mana mechanics make [Madness](https://en.wikipedia.org/wiki/List_of_Magic:_The_Gathering_keywords#Madness) too powerful'), what your idea is ('I think players should have to play madness cards by physically hurling them at their opponents. If you miss, your spell fizzles.'), and how it worked out when you playtested it ('My friend Jim lost an eye, but let's be honest, this is how fun happens!'). I can't promise your idea will be implemented or included, but I will at least read it, and I may be willing to discuss and/or refine it with you.